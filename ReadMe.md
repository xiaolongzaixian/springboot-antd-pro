<h1 align="center">Spring Boot 开发的 Antd Pro 后台</h1>

<div align="center">

</div>

## 依赖关系

#### Antd Pro 前端

Antd Pro Helper

[https://gitee.com/xiaolongzaixian/antd-pro-helper.git](https://gitee.com/xiaolongzaixian/antd-pro-helper.git) 

## 演示接口

Antd演示页面接口: AntdController.java

接口URL: /api/auth_routes
<br/>

Antd演示页面接口: AntdRestController.java

接口URL: /api/*
<br/>

跨域接口: AntdResController1.java

接口URL: /openapi/v1/*
<br/>

演示功能(CRUD): AntdResController2.java

接口URL: /openapi/v2/*

## 开发内容

#### 功能特性

- RESTful接口：演示功能(CRUD)

- 集成JWT，用于前后端实现Token鉴权

- 集成JPA，用于数据库操作实现

- 集成Druid，用于数据库连接池

#### 优化

暂无~

#### 漏洞修复

暂无~

## 快速开发

```bash
$ git clone https://gitee.com/xiaolongzaixian/springboot-antd-pro.git
$ cd springboot-antd-pro
$ mvn build
```

推荐IDE: Eclipse

## 编译运行

MySQL初始化脚本: /src/main/resources/sql/mysql/init.sql

MySQL配置参数: /src/main/resources/application.properties

应用启动程序: /src/main/java/javax/xianfeng/SpringAntdApplication.java

## 常见问题

#### SpringBoot开发后台接口返回数据格式

Antd组件的数据参数都支持扩展，可以结合项目情况进行定义

例如表格组件的JSON数据格式默认为

```bash
{
    list:[],
    pagination: {current: 1, pageSize: 10, total: 100}
}
```
