package javax.xianfeng;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @since 2020/01/03 20:25:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringAntdApplication.class)
public class SpringAntdApplicationTest {

	@Before
	public void init() {
		System.out.println(">>>" + this.getClass().getSimpleName() + " TEST BEGIN <<<");
	}
	
	@Test
	public void demo() {
		System.out.println("Hello World");
	}

	@After
	public void after() {
		System.out.println(">>>" + this.getClass().getSimpleName() + " TEST END <<<");
	}

}
